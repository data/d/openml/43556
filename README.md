# OpenML dataset: Running-Log-Insight

https://www.openml.org/d/43556

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Content
All data was collected via my Garmin GPS watch and uploaded to Garmin Connect via Bluetooth. There are gaps in my log, the largest being from summer of 2019 to about spring 2020 (I ran competitively in college and took a break when I graduated in 2019). My GPS also has a wrist heart rate monitor, although it isn't always accurate. Similarly, occasionally the GPS had some inaccuracies as well, though subjectively I felt this occurred less frequently.
Acknowledgements
Thank you to Garmin Connect for making it easy to export this data and to my legs.
Inspiration
Based on my training log data, can you estimate my "fitness" as a function of time? Specifically, when was my fitness maximized? I have a record of when I ran all of my PRs (personal records), so that could be a good way to check if you are right.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43556) of an [OpenML dataset](https://www.openml.org/d/43556). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43556/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43556/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43556/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

